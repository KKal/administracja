package common;


import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;

import view.CpuPanelFactory;
import view.DisksPanelFactory;
import view.UsbPanelFactory;
import view.WifiPanelFactory;

public class MainFrame extends JFrame{

	private static final long serialVersionUID = 3725707200640647576L;

	public MainFrame() {
		super("HARDWARE INFO PROVIDER");
		init();
	}
	
	
	/**
	 * Creates frame's layout and sets basic properties of window.
	 */
	private void init() {		
		createLayout();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1000, 600);
		setVisible(true);
	}
	
	
	/**
	 * Creates frame's layout.
	 */
	private void createLayout() {
		//Set Nimbus LookAndFeel.
		for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
			if ("Nimbus".equals(info.getName())) {
				try {
					UIManager.setLookAndFeel(info.getClassName());
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
							| UnsupportedLookAndFeelException e) {		
					e.printStackTrace();
				}
		        break;
		    }
		}

		JPanel panel = (JPanel) getContentPane();
		panel.setLayout(new BorderLayout());	
		
		//Create panels with hardware info.
		JPanel cpuPanel = new CpuPanelFactory().createPanel();
		JPanel diskPanel = new DisksPanelFactory().createPanel();
		JPanel wifiPanel = new WifiPanelFactory().createPanel();
		JPanel usbPanel = new UsbPanelFactory().createPanel();
		
		JScrollPane wifiScrollPane = new JScrollPane(wifiPanel, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		//Create tabs.
		JTabbedPane tabbedPane = new JTabbedPane();
		tabbedPane.addTab("CPU", cpuPanel);	
		tabbedPane.addTab("WI-FI", wifiScrollPane);
		tabbedPane.addTab("DISK", diskPanel);
		tabbedPane.addTab("USB", usbPanel);
		
		panel.add(tabbedPane);
	}
}
