package common;

import java.awt.Color;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesProvider {

	private Properties properties;
	
	public PropertiesProvider(String propertyFileName) {
		properties = new Properties();
		InputStream is = null;
		try {
			is = new FileInputStream(propertyFileName);
			properties.load(is);
		
		} catch (IOException ex) {
			ex.printStackTrace();
		
		} finally {
			if(is != null){
				try {
					is.close();	
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	
	/**
	 * Returns property by its name.
	 * @param property	name of property
	 */
	public String getProperty(String property) {
		return properties.getProperty(property);
	}
	
	
	/**
	 * Returns color property.
	 * @param property	name of property.
	 */
	public Color getColor(String property) {
		String colorHex = getProperty(property);
		Color color = null;
		try {
			color = colorHex != null ? Color.decode(colorHex) : new Color(28, 215, 222);
		} catch(NumberFormatException ex) {
			ex.printStackTrace();
			//Return default color.
			color = new Color(28, 215, 222);
		}
		return color;
	}
}
