package common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HardwareDataProvider {
	
	private String password;
	
	public HardwareDataProvider() {
		PropertiesProvider pp = new PropertiesProvider("resources//config.properties");
		password = pp.getProperty("userPassword");
	}
	
	/**
	 * Gets buffered reader for file.
	 * @param filepath	path to file
	 * @return			BufferedReader to read file
	 * @throws FileNotFoundException	when file is not found.
	 */
	public BufferedReader getFileReader(String filepath) throws FileNotFoundException {
		File file = new File("filepath");
		FileReader fileReader = new FileReader(file);
		return new BufferedReader(fileReader);
	}
	
	/**
	 * Gets CPU data.
	 * @return map where key is property's name and value is property's value or null if 
	 * map creation failed.
	 */
	public Map<String, String> getCpuInfo() {
		try {
			//Read cpuinfo file using process.
			Process process;
			process = Runtime.getRuntime().exec("cat /proc/cpuinfo");
			process.waitFor();
			BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
			
			//Parse data.
			String line;
			Map<String, String> cpuInfo = new HashMap<String, String>();
			while ((line = br.readLine()) != null) {
				String[] tokens = line.split(":");
				if(tokens.length == 2) {
					//Put data into map.
					cpuInfo.put(tokens[0].trim(), tokens[1].trim());
				} else if(tokens.length == 1) {
					//Put data into map.
					cpuInfo.put(tokens[0].trim(), "");
				}
			}
			return cpuInfo;
		
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
			return null;
		} 	
	}
	
	
	/**
	 * Gets disks' information.
	 * @return list of maps (one map for each disk) with disks' properties where key is property's name 
	 * and value is property's value or null if map's creation failed.
	 */
	public List<Map<String,String>> getDiskInfo() {
		try {
		    //Execute command parted -l using password.
		    String[] cmd2 = {"/bin/bash","-c","echo " + password + "| sudo -S parted -l"};
		    Process pb2 = Runtime.getRuntime().exec(cmd2);
		    pb2.waitFor();
		    
		    BufferedReader input2 = new BufferedReader(new InputStreamReader(pb2.getInputStream()));
		    
		    //Parse data.
		    String[] properties = {"Numer", "Początek", "Koniec", "Rozmiar", "Typ", 
		    	"System plików", "Flaga"};
		    
		    int[] propertiesIndexes = new int[properties.length];
		    Integer index1 = null;
		    Integer index2 = null;
		    
		    //Ignore first five lines.
		    for(int i = 0; i < 5; i++){
		    	input2.readLine();
		    }
		    
		    //Find properties location's in line.
		    String line = input2.readLine();
		    for(int i = 0; i < properties.length - 1; i++){
		    	index1 = line.indexOf(properties[i]);
		    	index2 = line.indexOf(properties[i+1]);
		    	propertiesIndexes[i] = index1;
		    	propertiesIndexes[i+1] = index2;
		    }
		    
		    List<Map<String,String>> disksInfo = new ArrayList<Map<String, String>>();
		    
		    while ((line = input2.readLine()).equals("") == false) {
		    	//Each line holds data for one disk.
		    	Map<String, String> propertiesMap = new HashMap<String, String>(0);
		    	for (int i = 0; i < properties.length - 1; i++) {
		    		//If there is no more properties in line, fill rest of the map with empty strings.
		    		if (line.length() - 1 < propertiesIndexes[i]) {
		    			for (int j = i; j < properties.length - 1; j++) {
		    				propertiesMap.put(properties[j], "");
		    			}
		    			break;
		    		} else {
		    			//Get property's value and put it in the map
		    			int idx2 = propertiesIndexes[i+1] > line.length()-1 ? line.length() : propertiesIndexes[i+1];
		    			String value = line.substring(propertiesIndexes[i], idx2);
		    			propertiesMap.put(properties[i], value.trim());
		    		}
		    	}
	    		if (line.length()-1 > propertiesIndexes[properties.length-1]) {
	    			//Get last property's value and put it in the map
	    	    	String value = line.substring(propertiesIndexes[properties.length-1], line.length());
	    	    	propertiesMap.put(properties[properties.length-1], value.trim());
	    		}
	    		//Add map with disk's info to list.
		    	disksInfo.add(propertiesMap);
		    }
		    input2.close();
		    
		    //Execute second command (fdisk -l).
		    String[] cmd = {"/bin/bash","-c","echo " + password + "| sudo -S fdisk -l"};
		    Process pb = Runtime.getRuntime().exec(cmd);
		    pb.waitFor();
	
		    BufferedReader input = new BufferedReader(new InputStreamReader(pb.getInputStream()));
		    List<String> data = new ArrayList<String>();
		    boolean dataStart = false;
		    
		    while ((line = input.readLine()) != null) {
		    	if (line.startsWith("Urządzenie"))
		    		dataStart = true;
		    	
		    	if (dataStart)
		    		data.add(line);
		    }
		    
		    List<String[]> dataParsed = new ArrayList<String[]>(0);
		    for (String d : data) {
		    	String[] split = d.split("  +");
		    	dataParsed.add(split);
		    	//For each disk's map.
		    	for (Map<String,String> diskInfo : disksInfo) {
		    		//Put new data into the map.
		    		String number = diskInfo.get("Numer");
		    		if (split[0].contains(number) ){
		    			diskInfo.put("Urządzenie", split[0]);
		    			diskInfo.put("System", split[split.length-1]);
		    		}
		    	}
		    }
		    return disksInfo;
		
		} catch (InterruptedException | IOException ex) {
			ex.printStackTrace();
			return null;
		}		
	}
	

	/**
	 * Get WI-FI info.
	 * @return list of maps with access point's properties or null if map's creation failed.
	 */
	public List<Map<String, String>> getWifiInfo() {
		try { 
			//Execute command (iwlist).
		    String[] cmd = {"/bin/bash","-c","echo " + password + "| sudo -S iwlist wlan1 scan"};
		    Process pb = Runtime.getRuntime().exec(cmd);
		    pb.waitFor();
	
		    BufferedReader input = new BufferedReader(new InputStreamReader(pb.getInputStream()));
		     
		    List<Map<String,String>> cells = new ArrayList<Map<String,String>>();
		    Map<String, String> tmpCell = new HashMap<String, String>(0);
		    //Ignore first line.
		    input.readLine();
		    
		    //Parse data.
		    String line;
		    while ((line = input.readLine()) != null) {
		    	if (line.startsWith("          Cell")) {
		    		if (!tmpCell.isEmpty())
		    			//Add map to list.
		    			cells.add(tmpCell);
		    		
		    		//Create map for access point's properties.
		    		tmpCell = new HashMap<String, String>();
		    		String firstLineData[] = line.split(" - ");
		    		
		    		//Add address.
		    		String address[] = firstLineData[1].split(": ");
		    		tmpCell.put(address[0], address[1]);
		    	}
		    	String lineData[] = line.split(":");
		    	if (lineData.length == 2)
		    		//Add property.
		    		tmpCell.put(lineData[0].trim(), lineData[1].trim());
		    }
		    input.close();
		    return cells;
		} catch (InterruptedException | IOException ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	
	/**
	 * Gets USB info.
	 * @return	map with USB's properties where key is property's name 
	 * and value is property's value or null if map's creation failed.
	 */
	public Map<String, String> getUsbInfo() {
		try {
			//Execute command lsusb.
			Process process = Runtime.getRuntime().exec("lsusb");
			process.waitFor();
			BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
			
			//Parse data.
			String line;	
			Map<String, String> usbInfo = new HashMap<String, String>();
			while((line = br.readLine()) != null){
				String[] splited = line.split(": ");		
				usbInfo.put(splited[0], splited[1]);
			}
			return usbInfo;
		
		} catch (InterruptedException | IOException ex) {
			ex.printStackTrace();
			return null;
		}
	}
}
