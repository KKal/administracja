package view;

import javax.swing.JPanel;

public interface IPanelFactory {
	public JPanel createPanel();
}
