package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import common.HardwareDataProvider;
import common.PropertiesProvider;

public class DisksPanelFactory implements IPanelFactory{

	@Override
	public JPanel createPanel() {
	
		PropertiesProvider pp = new PropertiesProvider("resources//config.properties");
		Color color1 = pp.getColor("color1");
		Color color2 = pp.getColor("color2");
		Color color3 = pp.getColor("color3");
		Color fontColor = pp.getColor("fontColor1");
		
		JPanel disksPanel = new JPanel();
		disksPanel.setLayout(new BorderLayout());
		
		JLabel headerLabel = new JLabel("DISKS INFO");
		Font f1 = new Font("Sans Serif",Font.BOLD, 30);
		headerLabel.setFont(f1);
		headerLabel.setForeground(fontColor);
		headerLabel.setHorizontalAlignment(SwingConstants.CENTER);
		headerLabel.setBackground(color3);
		headerLabel.setOpaque(true);
		headerLabel.setBorder(BorderFactory.createMatteBorder(1,1,2,1, Color.GRAY));
		disksPanel.add(headerLabel, BorderLayout.NORTH);
		
		JPanel diskInfoPanel = new JPanel();
		diskInfoPanel.setLayout(new GridLayout(0,5));
		
		JLabel label = new JLabel();
		Font f = new Font("Sans Serif",Font.BOLD, 18);
		label.setFont(f);
		label.setForeground(fontColor);
		label.setText("Urządzenie");
		label.setBorder(BorderFactory.createMatteBorder(0,0,2,0, Color.GRAY));
		label.setBackground(color3);
		label.setOpaque(true);
		diskInfoPanel.add(label);
		
		JLabel label4 = new JLabel();
		label4.setFont(f);
		label4.setForeground(fontColor);
		label4.setText("Rozmiar");
		label4.setBorder(BorderFactory.createMatteBorder(0,0,2,0, Color.GRAY));
		label4.setBackground(color3);
		label4.setOpaque(true);
		diskInfoPanel.add(label4);
		
		JLabel label3 = new JLabel();
		label3.setFont(f);
		label3.setForeground(fontColor);
		label3.setText("Rozruch");
		label3.setBorder(BorderFactory.createMatteBorder(0,0,2,0, Color.GRAY));
		label3.setBackground(color3);
		label3.setOpaque(true);
		diskInfoPanel.add(label3);

		JLabel label5 = new JLabel();
		label5.setFont(f);
		label5.setForeground(fontColor);
		label5.setText("System plików");
		label5.setBorder(BorderFactory.createMatteBorder(0,0,2,0, Color.GRAY));
		label5.setBackground(color3);
		label5.setOpaque(true);
		diskInfoPanel.add(label5);
		
		
		JLabel label6 = new JLabel();
		label6.setFont(f);
		label6.setForeground(fontColor);
		label6.setText("Typ");
		label6.setBorder(BorderFactory.createMatteBorder(0,0,2,0, Color.GRAY));
		label6.setBackground(color3);
		label6.setOpaque(true);
		diskInfoPanel.add(label6);
		
		HardwareDataProvider configProvider = new HardwareDataProvider();
		
		List<Map<String,String>> diskInfo = configProvider.getDiskInfo();
			
		for (int i = 0; i < diskInfo.size(); i++) {
			Color backgroundColor = i % 2 == 0 ? color1  : color2;
			Map<String,String> data = diskInfo.get(i);
			
			JLabel propLabel = new JLabel();
			propLabel.setText(data.get("Urządzenie"));
			propLabel.setBorder(BorderFactory.createMatteBorder(0,0,1,0, Color.GRAY));
			propLabel.setBackground(backgroundColor);
			propLabel.setOpaque(true);
			propLabel.setForeground(Color.BLACK);
			propLabel.setFont(new Font("Courier New", Font.BOLD, 17));
				

			JLabel propLabel4 = new JLabel();
			propLabel4.setText(data.get("Rozmiar"));
			propLabel4.setBorder(BorderFactory.createMatteBorder(0,0,1,0, Color.GRAY));
			propLabel4.setForeground(Color.BLACK);
			propLabel4.setOpaque(true);
			propLabel4.setBackground(backgroundColor);
			propLabel4.setFont(new Font("Courier New", Font.ITALIC, 17));
				
			JLabel propLabel3 = new JLabel();
			String boot = data.get("Flaga");
			String text = "";
			if(boot != null){
				text = boot.equals("ładowalna") ? "*" : "";
			}
			propLabel3.setText(text);
			propLabel3.setBorder(BorderFactory.createMatteBorder(0,0,1,0, Color.GRAY));
			propLabel3.setForeground(Color.BLACK);
			propLabel3.setFont(new Font("Courier New", Font.ITALIC, 17));
			propLabel3.setBackground(backgroundColor);
			propLabel3.setOpaque(true);
			
				
			JLabel propLabel5 = new JLabel();
			String boot5 = data.get("System plików");
			String text5 = "";
			if(boot5 != null){
				text5 = boot5;
			}
			propLabel5.setText(text5);
			propLabel5.setBorder(BorderFactory.createMatteBorder(0,0,1,0, Color.GRAY));
			propLabel5.setForeground(Color.BLACK);
			propLabel5.setFont(new Font("Courier New", Font.ITALIC, 17));
			propLabel5.setBackground(backgroundColor);
			propLabel5.setOpaque(true);
			
			JLabel propLabel6 = new JLabel();
			propLabel6.setText(data.get("Typ"));
			propLabel6.setBorder(BorderFactory.createMatteBorder(0,0,1,0, Color.GRAY));
			propLabel6.setForeground(Color.BLACK);
			propLabel6.setOpaque(true);
			propLabel6.setBackground(backgroundColor);
			propLabel6.setFont(new Font("Courier New", Font.ITALIC, 17));
				
			diskInfoPanel.add(propLabel);
			diskInfoPanel.add(propLabel4);
			diskInfoPanel.add(propLabel3);
			diskInfoPanel.add(propLabel5);
			diskInfoPanel.add(propLabel6);
		}
		disksPanel.add(diskInfoPanel, BorderLayout.CENTER);
		return disksPanel;
	}

}
