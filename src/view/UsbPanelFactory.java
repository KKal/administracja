package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import common.HardwareDataProvider;
import common.PropertiesProvider;

public class UsbPanelFactory implements IPanelFactory {

	@Override
	public JPanel createPanel() {
		PropertiesProvider pp = new PropertiesProvider("resources//config.properties");
		Color color1 = pp.getColor("color1");
		Color color2 = pp.getColor("color2");
		Color color3 = pp.getColor("color3");
		Color fontColor = pp.getColor("fontColor1");
		
		JPanel usbPanel = new JPanel();
		usbPanel.setLayout(new BorderLayout());
		
		JLabel headerLabel = new JLabel("USB INFO");
		Font f1 = new Font("Sans Serif",Font.BOLD, 30);
		headerLabel.setFont(f1);
		headerLabel.setForeground(fontColor);
		headerLabel.setHorizontalAlignment(SwingConstants.CENTER);
		headerLabel.setBackground(color3);
		headerLabel.setOpaque(true);
		usbPanel.add(headerLabel, BorderLayout.NORTH);
		
		JPanel usbInfoPanel = new JPanel();
		usbInfoPanel.setLayout(new GridLayout(0, 2));
		HardwareDataProvider configProvider = new HardwareDataProvider();

		Map<String, String> usbInfo = configProvider.getUsbInfo();
		
		int i = 0;
		for(Map.Entry<String,String> entry: usbInfo.entrySet()){	
			Color backgroundColor = i % 2 == 0 ? color1  : color2;
			
			JLabel label = new JLabel();
			label.setText(entry.getKey());
			Font f = new Font("Sans Serif", Font.BOLD, 14);
			label.setFont(f);
			label.setBorder(BorderFactory.createMatteBorder(1,0,1,0, Color.GRAY));
			label.setOpaque(true);
			label.setBackground(backgroundColor);
			usbInfoPanel.add(label);
			
			JLabel label2 = new JLabel();
			label2.setText(entry.getValue());
			Font f2 = new Font("Sans Serif", Font.PLAIN, 14);
			label2.setFont(f2);
			label2.setBorder(BorderFactory.createMatteBorder(1,0,1,0, Color.GRAY));
			label2.setOpaque(true);
			label2.setBackground(backgroundColor);
			usbInfoPanel.add(label2);
			
			i++;
		}
		usbPanel.add(usbInfoPanel, BorderLayout.CENTER);
		return usbPanel;
	}

}
