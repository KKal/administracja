package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import common.HardwareDataProvider;
import common.PropertiesProvider;

public class CpuPanelFactory implements IPanelFactory {

	@Override
	public JPanel createPanel() {
		PropertiesProvider pp = new PropertiesProvider("resources//config.properties");
		Color color1 = pp.getColor("color1");
		Color color2 = pp.getColor("color2");
		Color color3 = pp.getColor("color3");
		Color fontColor = pp.getColor("fontColor1");
		
		JPanel cpuPanel = new JPanel();
		cpuPanel.setLayout(new BorderLayout());
		
		JLabel headerLabel = new JLabel("CPU INFO");
		Font f1 = new Font("Sans Serif",Font.BOLD, 30);
		headerLabel.setFont(f1);
		headerLabel.setForeground(fontColor);
		headerLabel.setHorizontalAlignment(SwingConstants.CENTER);
		headerLabel.setBackground(color3);
		headerLabel.setOpaque(true);
		headerLabel.setBorder(BorderFactory.createMatteBorder(0,0,2,0, Color.GRAY));
		cpuPanel.add(headerLabel, BorderLayout.NORTH);
		
		JPanel cpuInfoPanel = new JPanel();
		cpuInfoPanel.setLayout(new GridLayout(0,2));
		JLabel label = new JLabel();
		Font f2 = new Font("Sans Serif",Font.BOLD, 15);
		label.setFont(f2);
		
		HardwareDataProvider configProvider = new HardwareDataProvider();
		
		configProvider.getDiskInfo();

		String[] cpuProperties = {"cpu cores", "model name", "cache size", "cpu MHz", "address sizes", "stepping", "fpu"};
		
		Map<String,String> cpuInfo = configProvider.getCpuInfo();
		int i = 0;
		for(String prop : cpuProperties){
			Color backgroundColor = i % 2 == 0 ? color1  : color2;
				
			JLabel propLabel = new JLabel();
			propLabel.setHorizontalAlignment(SwingConstants.RIGHT);
			propLabel.setText(prop + ":      ");
			propLabel.setBorder(BorderFactory.createMatteBorder(0,0,1,0, Color.GRAY));
			propLabel.setAlignmentX(0);
			propLabel.setForeground(Color.BLACK);	
			propLabel.setFont(new Font("Courier New", Font.BOLD, 17));
			propLabel.setBackground(backgroundColor);
			propLabel.setOpaque(true);
			
			JLabel info = new JLabel();
			info.setText("      " + cpuInfo.get(prop));
			info.setBorder(BorderFactory.createMatteBorder(0,0,1,0, Color.GRAY));
			info.setFont(new Font("Courier New", Font.ITALIC, 17));
			info.setBackground(backgroundColor);
			info.setOpaque(true);
				
			cpuInfoPanel.add(propLabel);
			cpuInfoPanel.add(info);
			i++;
		}

		cpuInfoPanel.add(label);
		cpuPanel.add(cpuInfoPanel, BorderLayout.CENTER);
		return cpuPanel;		
	}

}
