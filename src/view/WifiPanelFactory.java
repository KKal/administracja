package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import common.HardwareDataProvider;
import common.PropertiesProvider;

public class WifiPanelFactory implements IPanelFactory{

	@Override
	public JPanel createPanel() {
		PropertiesProvider pp = new PropertiesProvider("resources//config.properties");
		Color color1 = pp.getColor("color1");
		Color color2 = pp.getColor("color2");
		Color color3 = pp.getColor("color3");
		Color fontColor = pp.getColor("fontColor1");
		
		JPanel wifiPanel = new JPanel();
		wifiPanel.setLayout(new BorderLayout());
		
		JLabel headerLabel = new JLabel("WI-FI INFO");
		Font f1 = new Font("Sans Serif",Font.BOLD, 30);
		headerLabel.setFont(f1);
		headerLabel.setForeground(fontColor);
		headerLabel.setHorizontalAlignment(SwingConstants.CENTER);
		headerLabel.setBackground(color3);
		headerLabel.setOpaque(true);
		headerLabel.setBorder(BorderFactory.createMatteBorder(1,1,2,1, Color.GRAY));
		wifiPanel.add(headerLabel, BorderLayout.NORTH);
		
		HardwareDataProvider cp = new HardwareDataProvider();
			
		JPanel wifiInfoPanel = new JPanel();			
		wifiInfoPanel.setLayout(new GridLayout(0,1));
		JPanel tmpPanel;
		List<Map<String, String>> wifiInfo = cp.getWifiInfo();
			
		int i = 0;
		for(Map<String, String> cell : wifiInfo){
			Color backgroundColor = i % 2 == 0 ? color1  : color2;
			tmpPanel = new JPanel();
			tmpPanel.setBorder(BorderFactory.createMatteBorder(1,1,1,1, Color.GRAY));
			tmpPanel.setLayout(new GridLayout(0,2));
			
			for(Map.Entry<String, String>entry : cell.entrySet()){
				JLabel label = new JLabel();
				label.setText(entry.getKey());
				Font f = new Font("Sans Serif",Font.BOLD, 14);
				label.setFont(f);
				label.setOpaque(true);
				label.setBackground(backgroundColor);
				tmpPanel.add(label);
					
					
				JLabel label2 = new JLabel();
				label2.setText(entry.getValue());
				Font f2 = new Font("Sans Serif", Font.PLAIN, 14);
				label2.setFont(f2);
				label2.setOpaque(true);
				label2.setBackground(backgroundColor);
				tmpPanel.add(label2);
			}
			wifiInfoPanel.add(tmpPanel);
			i++;
		}
		wifiPanel.add(wifiInfoPanel, BorderLayout.CENTER);
		return wifiPanel;
	}

}
